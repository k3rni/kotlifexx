sealed class Cell {
    class Alive : Cell() {
        override fun toString() = "A"
    }
    class Dead : Cell() {
        override fun toString(): String = "D"
    }
}

interface Board {
    fun get(x: Int, y: Int) : Cell
    fun set(x: Int, y: Int, cell: Cell)

    fun step()
}