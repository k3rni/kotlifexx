import jquery.MouseClickEvent
import org.w3c.dom.events.Event
import org.w3c.dom.events.MouseEvent
import kotlin.browser.document
import kotlin.browser.window

fun main(args: Array<String>) {
    val canvas = document.getElementById("life")
    val ctx = canvas.asDynamic().getContext("2d")
    val board = MapBoard(ConwayClassicRule())

    val life = LifeCanvas(ctx, board)
    life.clear()
    life.draw()

    canvas.asDynamic().onclick = { event: MouseEvent ->
        val x = event.clientX / life.scale
        val y = event.clientY / life.scale
        board.spawnRandomGlider(x.toInt(), y.toInt())
        life.draw()
    }

    var timer: Int = 0

    document.getElementById("start")?.addEventListener("click", {
        timer = window.setInterval({
            board.step()
            life.draw()
        }, 50)
    })

    document.getElementById("step")?.addEventListener("click", {
        board.step()
        life.draw()
    })

    document.getElementById("stop")?.addEventListener("click", {
        window.clearInterval(timer)
        timer = 0
    })
}