interface Spawning {
    fun seedRandomly(width: Int, height: Int)
    fun spawnRandomGlider(x: Int, y: Int)
}