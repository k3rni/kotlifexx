class ConwayClassicRule : Ruleset {
    override fun survives(neighborCount: Int): Boolean {
        return (neighborCount == 2 || neighborCount == 3)
    }

    override fun spawns(neighborCount: Int): Boolean {
        return (neighborCount == 3)
    }
}