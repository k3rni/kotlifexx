interface Ruleset {
    fun survives(neighborCount: Int) : Boolean
    fun spawns(neighborCount: Int) : Boolean
}