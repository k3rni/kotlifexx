import org.w3c.dom.CanvasRenderingContext2D

class LifeCanvas(val ctx: CanvasRenderingContext2D, val board: Board) {
    private val width = ctx.canvas.width
    private val height = ctx.canvas.height
    private var top = 0
    private var left = 0
    var scale = 4.0

    fun clear() {
        ctx.fillStyle = "#000000"
        ctx.fillRect(0.0, 0.0, width.toDouble(), height.toDouble())
    }

    fun draw() {
        clear()
        ctx.fillStyle = "#FFFFFF"
        for (y in (0..height))
            for (x in (0..width)) {
                val state = board.get(left + x, top + y)
                if (state is Cell.Dead) continue
                ctx.fillRect(x * scale, y * scale, scale, scale)
            }
    }
}