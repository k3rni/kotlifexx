import kotlin.js.Math

typealias Coords = Pair<Int, Int>

class MapBoard(val ruleset: Ruleset) : Board, Spawning {
    val cellmap : MutableMap<Coords, Boolean> = HashMap()

    override fun get(x: Int, y: Int): Cell {
        return when (cellmap[x to y]) {
            true -> Cell.Alive()
            else -> Cell.Dead()
        }
    }

    override fun set(x: Int, y: Int, cell: Cell) {
        val addr = x to y
        when (cell) {
            is Cell.Alive -> cellmap[addr] = true
            is Cell.Dead -> cellmap.remove(addr)
        }
    }

    override fun seedRandomly(width: Int, height: Int) {
        (0..height).forEach { y ->
            (0..width).forEach { x ->
                if (Math.random() < 0.3) set(x, y, Cell.Alive())
            }
        }
    }

    override fun step() {
        val kill: MutableSet<Coords> = mutableSetOf()
        val spawn: MutableSet<Coords> = mutableSetOf()

        cellmap.forEach { entry ->
            val (coords, state) = entry
            // Some points will be calculated multiple times. Such is Life™
            val (cx, cy) = coords
            (cx - 1 .. cx + 1).forEach { x ->
                (cy - 1 .. cy + 1).forEach { y ->
                    val nc = liveNeighborCount(x, y)
                    val cell = get(x, y)
//                    console.log("[$x, $y]($cell) -> $nc")
                    when (cell) {
                        is Cell.Alive -> {
                            if (!ruleset.survives(nc)) kill.add(x to y)
                            // else: don't touch it
                        }
                        is Cell.Dead -> {
                            if (ruleset.spawns(nc)) spawn.add(x to y)
                        }
                    }
                }
            }
        }

        kill.forEach { cellmap.remove(it) }
        spawn.forEach { cellmap[it] = true }
    }

    fun liveNeighborCount(x: Int, y: Int): Int {
        var count = 0
        for (nx in (x - 1 .. x + 1))
            for (ny in (y - 1 .. y + 1)) {
                if (nx == x && ny == y) continue
                cellmap.get(nx to ny)?.let { count += 1 }
            }
        return count
    }

    override fun spawnRandomGlider(x: Int, y: Int) {
        val glider = listOf(listOf(false, true, false), listOf(false, false, true), listOf(true, true, true))
        var g: List<List<Boolean>> = glider

        if (Math.random() < 0.5)
            g = glider.map { row -> row.reversed() }
        if (Math.random() < 0.5)
            g = g.reversed()

        g.forEachIndexed { i, row ->
            row.forEachIndexed { j, b -> set(x + j, y + i, if (b) Cell.Alive() else Cell.Dead()) }
        }
    }
}